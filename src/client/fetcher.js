export default (url, options) => {
  return new Promise((resolve, reject) => {
    let status = 500;

    fetch(url, options)
      .then((response) => {
        status = response.status;

        return response.json();
      })
      .then((data) => {
        if (/^2\d\d$/.test(status)) {
          resolve({
            status,
            data,
          });
        } else {
          reject({
            status,
            data,
          });
        }
      })
      .catch((error) => {
        reject({
          status,
          data: error,
        });
      });
  });
}
