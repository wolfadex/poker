import React, { Component } from 'react';
import './styles.less';

export default class PersonPic extends Component {
  render() {
    const {
      image,
    } = this.props;

    if (image) {
      return (
        <div
          className="square-image"
          style={{
            backgroundImage: `url(${image})`,
          }}
        />
      );
    }

    return (
      <div className="square-image empty-image">
        ?
      </div>
    );
  }
}
