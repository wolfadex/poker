import '@babel/polyfill';
import 'file-loader?name=[name].[ext]!./index.pug';
import React from 'react';
import { render } from 'react-dom';
import App from './containers/App';

render(
  <App />,
  document.getElementById('root'),
);
