import React, { Component } from 'react';
import {
  Table,
  Button,
  Icon,
} from 'semantic-ui-react';
import PersonPic from '../../components/PersonPic';
import 'flag-icon-css/less/flag-icon.less';
import './styles.less';
import Edit from '../Edit';

const winningsFormatter = Intl.NumberFormat('en-US', {
  style: 'currency',
  currency: 'USD',
});

export default class Player extends Component {
  state = {
    showEdit: false,
  }

  handleDelete = () => {
    this.props.removePlayer(this.props.id)
  }

  startEdit = () => {
    this.setState({
      showEdit: true,
    });
  }

  handleClose = () => {
    this.setState({
      showEdit: false,
    });
  }

  render() {
    const {
      rank,
      name,
      winnings,
      country = '',
      profile_image,
      updatePlayer,
    } = this.props;
    const {
      showEdit,
    } = this.state;

    return (
      <Table.Row>
        <Table.Cell>
          {
            showEdit &&
            <Edit {...this.props} updatePlayer={updatePlayer} handleClose={this.handleClose} />
          }
          <div className="person-column">
            <span className="player-rank">
              {rank}.
            </span>
            <PersonPic image={profile_image} />
            <span className="player-name">
              {name}
            </span>
          </div>
        </Table.Cell>
        <Table.Cell textAlign="right">
          {winningsFormatter.format(winnings)}
        </Table.Cell>
        <Table.Cell textAlign="center">
          <div className="country-column">
            <span className={`flag-icon flag-icon-${country.toLowerCase()}`}/>
            <span className="country-initials">
              {country}
            </span>
            <Button.Group>
              <Button
                icon
                onClick={this.startEdit}
              >
                <Icon name='edit' />
              </Button>
              <Button
                icon
                onClick={this.handleDelete}
              >
                <Icon name='delete' />
              </Button>
            </Button.Group>
          </div>
        </Table.Cell>
      </Table.Row>
    );
  }
}
