import React, { Component } from 'react';
import {
  Button,
  Icon,
  Modal,
  Form,
} from 'semantic-ui-react';
import fetcher from '../../fetcher';

const LOADING_STATE = {
  IN_PROGRESS: 'IN_PROGRESS',
  SUCCESS: 'SUCCESS',
  FAILRUE: 'FAILRUE',
};

export default class Add extends Component {
  state = {
    form: {
      name: '',
      winnings: 0,
      country: '',
      profile_image: '',
    },
    loading: LOADING_STATE.SUCCESS,
  }

  handleValueChange = (e) => {
    e.persist();
    this.setState(({ form }) => ({
      form: {
        ...form,
        [e.target.name]: e.target.value,
      },
    }));
  }

  handleSubmit = () => {
    this.setState({
      loading: LOADING_STATE.IN_PROGRESS,
    });
    console.log('carl - 1');
    fetcher('player', {
      method: 'POST',
      headers: {
        'content-type': 'application/json'
      },
      body: JSON.stringify(this.state.form),
    })
      .then(({ data }) => {
        this.setState({
          loading: LOADING_STATE.SUCCESS,
        });
        console.log('carl - 2');
        this.props.updatePlayer();
        this.props.handleClose();
      })
      .catch(() => {
        this.setState({
          loading: LOADING_STATE.FAILRUE,
        });
      });
  }

  render() {
    const {
      form: {
        name,
        winnings,
        country,
        profile_image,
      },
      loading,
    } = this.state;
    const {
      open,
      handleClose,
    } = this.props;

    return (
      <Modal
        size="small"
        open
        onSubmit={this.handleSubmit}
      >
        <Modal.Header>
          Add New Player
        </Modal.Header>
        <Modal.Content>
          <Form loading={loading === LOADING_STATE.IN_PROGRESS}>
            <Form.Field>
              <label>Name</label>
              <input
                required
                pattern=".{1,}"
                title="A name is required"
                name="name"
                value={name}
                onChange={this.handleValueChange}
              />
            </Form.Field>
            <Form.Field>
              <label>Country</label>
              <input
                required
                pattern=".{2}"
                title="A 2 letter counter code is required"
                name="country"
                value={country}
                onChange={this.handleValueChange}
              />
            </Form.Field>
            <Form.Field>
              <label>Profile Image URL</label>
              <input
                name="profile_image"
                value={profile_image}
                onChange={this.handleValueChange}
              />
            </Form.Field>
            <Form.Field>
              <label>Winnings</label>
              <input
                type="number"
                required
                min="0"
                title="Winnings must be greater than 0"
                name="winnings"
                value={winnings}
                onChange={this.handleValueChange}
              />
            </Form.Field>
            <Form.Group>
              <Form.Button content="Submit New Player" />
              <Form.Button content="Cancel" type="button" onClick={handleClose} />
            </Form.Group>
          </Form>
        </Modal.Content>
      </Modal>
    );
  }
}
