import React, { Component } from 'react';
import { Table } from 'semantic-ui-react';
import Player from '../Player';
import './styles.less';

export default class List extends Component {
  render() {
    const {
      players = [],
      updatePlayer,
      removePlayer,
    } = this.props;

    return (
      <Table stackable>
        <Table.Header>
          <Table.Row>
            <Table.HeaderCell>
              Player
            </Table.HeaderCell>
            <Table.HeaderCell textAlign='right'>
              Winnings
            </Table.HeaderCell>
            <Table.HeaderCell textAlign="center">
              Native Of
            </Table.HeaderCell>
          </Table.Row>
        </Table.Header>
        <Table.Body>
          {
            players.length === 0 &&
            <Table.Row>
              Empty
            </Table.Row>
          }
          {players.map((player, i) =>
            <Player
              key={player.id}
              {...player}
              rank={i + 1}
              updatePlayer={updatePlayer}
              removePlayer={removePlayer}
            />
          )}
        </Table.Body>
      </Table>
    );
  }
}
