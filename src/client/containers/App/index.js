import React, { Component } from 'react';
import { Button } from 'semantic-ui-react';
import List from '../List';
import Add from '../Add';
import './styles.less';
import fetcher from '../../fetcher';

const LOADING_STATE = {
  IN_PROGRESS: 'IN_PROGRESS',
  SUCCESS: 'SUCCESS',
  FAILRUE: 'FAILRUE',
};

export default class App extends Component {
  state = {
    players: [],
    loading: LOADING_STATE.SUCCESS,
    showAdd: false,
  }

  componentDidMount() {
    this.getPlayers();
  }

  getPlayers = () => {
    this.setState({
      loading: LOADING_STATE.IN_PROGRESS,
    });

    fetcher('/player')
      .then(({ data }) => {
        this.setState({
          loading: LOADING_STATE.SUCCESS,
          players: data,
        });
      })
      .catch(({ data }) => {
        this.setState({
          loading: LOADING_STATE.FAILRUE,
        });
      });
  }

  updatePlayer = () => {
    this.getPlayers();
  }

  removePlayer = (playerId) => {
    this.setState({
      loading: LOADING_STATE.IN_PROGRESS,
    });

    fetch(`player/${playerId}`, {
      method: 'DELETE',
    })
      .then((data) => {
        this.getPlayers();
      })
      .catch((error) => {
        this.setState({
          loading: LOADING_STATE.FAILRUE,
        });
      });
  }

  toggleAdd = () => {
    console.log('carl');
    this.setState(({ showAdd }) => ({
      showAdd: !showAdd,
    }));
  }

  render() {
    const {
      players,
      loading,
      showAdd,
    } = this.state;

    return (
      <div className="app">
        <div className="title">
          ALL-TIME TOURNAMENT EARNINGS
          <Button
            primary
            onClick={this.toggleAdd}
            style={{
              position: 'absolute',
              right: '1rem',
            }}
          >
            New Player
          </Button>
          {
            showAdd &&
            <Add
              updatePlayer={this.updatePlayer}
              handleClose={this.toggleAdd}
            />
          }
        </div>
        {
          loading === LOADING_STATE.IN_PROGRESS &&
          'Loading...'
        }
        {
          loading === LOADING_STATE.FAILURE &&
          'We\'re sorry but an error has occured'
        }
        {
          loading === LOADING_STATE.SUCCESS &&
          <List
            players={players}
            updatePlayer={this.updatePlayer}
            removePlayer={this.removePlayer}
          />
        }
      </div>
    );
  }
}
