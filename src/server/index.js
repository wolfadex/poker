const path = require('path');
const express = require('express');
const uuid = require('uuid/v4');
let playerData = require('./playerData.json');

const app = express();
const errorResponse = (message) => ({
  error: `Error: ${message}`,
});

app.set('view engine', 'pug');
app.set('views', path.join(__dirname, '../../dist'));

app.use(express.json());
app.use(express.static(path.join(__dirname, '../../dist')));

app.get('/', (req, res) => res.render('index'));

// If this were a database query it'd be more of the form
// Tables: Player, Winnings
// Player Table Columns: id (primary key), name, country
// Winnings Table Columns: id (same as ^), amount
//
// Maybe the Winnings table would needs to be summed by id to get the total?
// Then sorted by total winnings probably
app.get('/player', (req, res) =>
  res
    .status(200)
    .send(playerData.sort((a, b) => b.winnings > a.winnings ? 1 : -1)));
app.get('/player/:playerId', (req, res) => {
  const { playerId } = req.params;
  const player = playerData.find(({ id }) => id === playerId);

  if (player == null) {
    res
      .status(404)
      .send(errorResponse('Player not found'));
  } else {
    res
      .status(200)
      .send(playerData.sort((a, b) => b.winnings > a.winnings ? 1 : -1));
  }
});
app.post('/player', (req, res) => {
  const {
    name,
    winnings,
    country,
    profile_image = '',
  } = req.body || {};
  let winningsParsed;

  try {
    winningsParsed = parseFloat(winnings);
  } catch (e) {
    res
      .status(400)
      .send(errorResponse(`"winnings" is a required field. Expected a Number greater than 0, got ${winnings}.`));
  }

  if (name == null || name === '') {
    res
      .status(400)
      .send(errorResponse(`"name" is a required field. Expected a String with a length greater than 0, got ${name}.`));
  } else if (winnings == null) {
    res
      .status(400)
      .send(errorResponse(`"winnings" is a required field. Expected a Number greater than 0, got ${winnings}.`));
  } else if (country == null || country === '') {
    res
      .status(400)
      .send(errorResponse(`"country" is a required field. Expected a String with a length of 2, got ${country}.`));
  } else {
    const newPerson = {
      name,
      winnings,
      country,
      profile_image,
      id: uuid(),
    };

    playerData.push(newPerson);

    res
      .status(200)
      .send(newPerson);
  }
});
app.put('/player/:playerId', (req, res) => {
  const { playerId } = req.params;
  const player = playerData.find(({ id }) => id === playerId);

  if (player == null) {
    res
      .status(404)
      .send(errorResponse('Player not found'));
  } else {
    const {
      name,
      winnings,
      country,
      profile_image = '',
      ...other
    } = req.body || {};
    let winningsParsed;

    try {
      winningsParsed = parseFloat(winnings);
    } catch (e) {
      res
        .status(400)
        .send(errorResponse(`"winnings" is a required field. Expected a Number greater than 0, got ${winnings}.`));
    }

    if (Object.keys(other).length > 0) {
      res
        .status(400)
        .send(errorResponse(`Too many keys we're sent. Expected one or all of "name", "winnings", "country", "profile_image" but additionally found ${JSON.stringify(other)}`));
    } else if (name == null || name === '') {
      res
        .status(400)
        .send(errorResponse(`"name" is a required field. Expected a String with a length greater than 0, got ${name}.`));
    } else if (winnings == null) {
      res
        .status(400)
        .send(errorResponse(`"winnings" is a required field. Expected a Number greater than 0, got ${winnings}.`));
    } else if (country == null || country === '') {
      res
        .status(400)
        .send(errorResponse(`"country" is a required field. Expected a String with a length of 2, got ${country}.`));
    } else {
      const updatedPerson = {
        ...player,
        name,
        winnings,
        country,
        profile_image,
      };

      playerData = playerData.map((player) => player.id === playerId ? updatedPerson : player);

      res
        .status(200)
        .send(updatedPerson);
    }
  }
});
app.delete('/player/:playerId', (req, res) => {
  const { playerId } = req.params;
  const preLength = playerData.length;
  playerData = playerData.filter(({ id }) => id !== playerId);
  const postLength = playerData.length;

  if (preLength === postLength) {
    res
      .status(404)
      .send(errorResponse('Player not found'));
  } else {
    res
      .status(200)
      .send({});
  }
});

app.listen(3000, () => console.log('Poker app listening on port 3000!'));
