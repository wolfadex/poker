# Poker Rankings

To run:

```
yarn install
yarn build
yarn start
```

then open a tab in your browser and navigate to `http://localhost:3000/`



### Notes
- Initial data is randomized.
- Players are ranked by winnings, largest to smallest.
- Person's photos are images of random size.
